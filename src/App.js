import { useState, useEffect } from "react";
import "./App.css";

const App = () => {
  const [todo, setTodo] = useState("");
  const [seconds, setSeconds] = useState(5);
  const [todos, setTodos] = useState(() => {
    const saveTodos = localStorage.getItem("todos");
    if (saveTodos) {
      return JSON.parse(saveTodos);
    } else {
      return [];
    }
  });

  //
  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(todos));
  }, [todos]);

  useEffect(() => {
    if (seconds > 0) {
      setTimeout(() => setSeconds(seconds - 1), 1000);
    } else {
      setTimeout(() => (setSeconds(seconds + 5),
      handleDeleteClick()), 1000);
    }
  });


  //
  function handleInputChange(e) {
    setTodo(e.target.value);
  }

  function handleFormSubmit(e) {
    e.preventDefault();
    if (todo !== "") {
      setTodos([
        ...todos,
        {
          // id: todos.length + 1,
          text: todo.trim(),
        },
      ]);
    }
    setTodo("");
  }

  function handleDeleteClick() {
    var removeItem =[]
    for(let i =1;i< todos.length;i++){
        removeItem.push(todos[i])
    }
  setTodos(removeItem);
  }

  // function handleDeleteAuto() {
  //   var id = null;
  //   // const removeItem = todos.sort((a, b) => {
  //   //   return a.id < b.id ? 1 : -1;
  //   // });
  //   for (let i = 0; i < todos.length; i++) {
  //     // console.log("dd", todos[i]);
  //     if (i === 0) {
  //       return (id = todos[i].id);
  //     }
  //   }
  //   const removeItem = todos.filter((todo) => {
  //     return todo.id !== id;
  //   });
  //   console.log("dd", todos);

  //   setTodos(removeItem);
  // }
  return (
    <div className="App">
      <form onSubmit={handleFormSubmit}>
        <input
          type="text"
          name="todo"
          placeholder="creat a new add"
          value={todo}
          onChange={handleInputChange}
        />
        <button value="Submit">Enter</button>
      </form>
      <div className="todo-list">
        {todos.map((todo) => (
          <p key={todo.text}>
            {todo.text}
            {/* <button onClick={() => handleDeleteClick(todo.id)}>x</button> */}
          </p>
        ))}
      </div>
    </div>
  );
};
export default App;
